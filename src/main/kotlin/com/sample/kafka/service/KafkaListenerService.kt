package com.sample.kafka.service

import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Service

@Service
class KafkaListenerService() {
    private val logger = LoggerFactory.getLogger(javaClass)

    @SendTo("bilyoner-test-2")
    @KafkaListener(topics = ["bilyoner-test-1"])
    fun consumeTopic1(id: String): String {
        logger.info("Message received to bilyoner-test-1. Id: {}", id)
        return id
    }

    @SendTo("bilyoner-test-3")
    @KafkaListener(topics = ["bilyoner-test-2"])
    fun consumeTopic2(id: String): String {
        logger.info("Message received to bilyoner-test-2. Id: {}", id)
        return id
    }

    @KafkaListener(topics = ["bilyoner-test-3"])
    fun consumeTopic3(id: String) {
        logger.info("Message received to bilyoner-test-3. Id: {}", id)
    }
}
