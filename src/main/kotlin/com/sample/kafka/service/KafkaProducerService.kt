package com.sample.kafka.service

import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class KafkaProducerService(
    private val kafkaTemplate: KafkaTemplate<String, Any>
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun publish(topic: String, body: Any) {
        logger.info("Topic: {} - Message: {}", topic, body)
        kafkaTemplate.send(ProducerRecord(topic, body))
    }
}
