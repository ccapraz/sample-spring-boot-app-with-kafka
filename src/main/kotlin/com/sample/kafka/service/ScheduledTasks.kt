package com.sample.kafka.service

import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class ScheduledTasks(
    private val kafkaProducerService: KafkaProducerService
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @Scheduled(fixedRate = 1000L)
    fun sendMessage() {
        val id = UUID.randomUUID().toString()
        logger.info("Scheduler is publishing a message with id {} to bilyoner-test-1 topic.", id)
        kafkaProducerService.publish("bilyoner-test-1", id)
    }
}
